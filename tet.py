## TET 
## Bu de tan sheng
## geemu o hajimeyou

import sys
import random

lettertonumber = "abcdefgh"
startboard_black = ["R","N","B","Q","K","B","N","R",
                    "P","P","P","P","P","P","P","P",
                    " "," "," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    "p","p","p","p","p","p","p","p",
                    "r","n","b","q","k","b","n","r",
                    True,True,True,True,0,0]
startboard_white = ["R","N","B","Q","K","B","N","R",
                    "P"," ","P","P","P","P","P","P",
                    " ","P"," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    " "," "," "," "," "," "," "," ",
                    "p","p","p","p","p","p","p","p",
                    "r","n","b","q","k","b","n","r",
                    True,True,True,True,0,0]
white_normal_start = ["b2b3","c2c3","d2d3","d1c2","g2g3","f2f3","e2e3","e1f2","h2h3","a2a3","c2d1","d1c2","c2d1",
                    "d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2",
                    "c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2",
                    "c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2",
                    "c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2","c2d1","d1c2"]
black_normal_start = ["b7b6","c7c6","d7d6","d8c7","g7g6","f7f6","e7e6","e8f7","h7h6","a7a6","c7d8","d8c7","c7d8",
                    "d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7",
                    "c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7",
                    "c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7",
                    "c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7","c7d8","d8c7"]

def evaluate_function_op(board,color):
    score = 0
    white_pawn_control = []
    white_rook_control = []
    white_knight_control = []
    white_bishop_control = []
    white_queen_control = []
    white_king_control = []
    black_pawn_control = []
    black_rook_control = []
    black_knight_control = []
    black_bishop_control = []
    black_queen_control = []
    black_king_control = []
    for i in range(64):
        if board[i] == "P":
            white_pawn_control = white_pawn_control + pawn_control(i,board,True)
        if board[i] == "R":
            white_rook_control = white_rook_control + rook_control(i,board,True)
        if board[i] == "N":
            white_knight_control = white_knight_control + knight_control(i,board,True)
        if board[i] == "B":
            white_bishop_control = white_bishop_control + bishop_control(i,board,True)
        if board[i] == "Q":
            white_queen_control = white_queen_control + queen_control(i,board,True)
        if board[i] == "K":
            white_king_control = white_king_control + king_control(i,board,True)
        if board[i] == "p":
            black_pawn_control = black_pawn_control + pawn_control(i,board,False)
        if board[i] == "r":
            black_rook_control = black_rook_control + rook_control(i,board,False)
        if board[i] == "n":
            black_knight_control = black_knight_control + knight_control(i,board,False)
        if board[i] == "b":
            black_bishop_control = black_bishop_control + bishop_control(i,board,False)
        if board[i] == "q":
            black_queen_control = black_queen_control + queen_control(i,board,False)
        if board[i] == "k":
            black_king_control = black_king_control + king_control(i,board,False)
    if color == True:
        control_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        danger_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "P":
                if i in danger_position:
                    modify1 = 0.3
                if i in control_position:
                    modify2 = 1.5
                if row == 3:
                    modify3 = 1.2
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "K":
                if row in [1,2]:
                    modify1 = 1.4
                score = score + 150 * modify1
            elif piece == "Q":
                if row == 1:
                    modify1 = 1.1
                elif row == 2:
                    modify1 = 1.05
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "RNBQKP":
                            modify2 = 1
                        elif board[j] in "rnbqkp":
                            modify2 = 0.2
                        else:
                            modify2 = 0.4
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.2
                    else:
                        modify3 = 0
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
            elif piece == "R":
                if row in [1,8]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify1 * modify2
            elif piece == "N":
                if col >= 2:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify1 * modify2
            elif piece == "B":
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify2

            elif board[i] == "p":
                if i-8 in white_queen_control:
                    score = score - 20
                if row == 4:
                    modify1 == 1.2
                if row == 3:
                    modify1 == 1.4
                if row == 2:
                    if board[i-8] == " ":
                        modify1 == 5
                    elif col >= 2:
                        if board[i-9] in "PRNBQK":
                            modify1 == 5
                    elif col <= 7:
                        if board[i-7] in "PRNBQK":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "k":
                if i in control_position:
                    score = score + 20
                score = score - 150
            elif board[i] == "q":
                score = score - 100
            elif board[i] == "r":
                score = score - 51
            elif board[i] == "n":
                score = score - 49
            elif board[i] == "b":
               score = score - 50
        return score
    if color == False:
        control_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        danger_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "p":
                if i in danger_position:
                    modify1 = 0.3
                if i in control_position:
                    modify2 = 1.5
                if row == 6:
                    modify3 = 1.1
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "k":
                if row in [7,8]:
                    modify1 = 1.4
                score = score + 150 * modify1
            elif piece == "q":
                if row == 8:
                    modify1 = 1.1
                elif row == 7:
                    modify1 = 1.05
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "RNBQKP":
                            modify2 = 0.2
                        elif board[j] in "rnbqkp":
                            modify2 = 1
                        else:
                            modify2 = 0.4
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.2
                    else:
                        modify3 = 0
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
            elif piece == "r":
                if row in [1,8]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify1 * modify2
            elif piece == "n":
                if col <= 7:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify1 * modify2
            elif piece == "b":
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.2
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.1
                score = score + 50 * modify2

            elif board[i] == "P":
                if i+8 in black_queen_control:
                    score = score - 20
                if row == 5:
                    modify1 == 1.2
                if row == 6:
                    modify1 == 1.4
                if row == 7:
                    if board[i-8] == " ":
                        modify1 == 5
                    if col >= 2:
                        if board[i-9] in "prnbqk":
                            modify1 == 5
                    if col <= 7:
                        if board[i-7] in "prnbqk":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "K":
                score = score - 150
            elif board[i] == "Q":
                score = score - 100
            elif board[i] == "R":
                score = score - 51
            elif board[i] == "N":
                score = score - 49
            elif board[i] == "B":
               score = score - 50
        return score

def evaluate_function_md(board,color):
    score = 0
    white_pawn_control = []
    white_rook_control = []
    white_knight_control = []
    white_bishop_control = []
    white_queen_control = []
    white_king_control = []
    black_pawn_control = []
    black_rook_control = []
    black_knight_control = []
    black_bishop_control = []
    black_queen_control = []
    black_king_control = []
    for i in range(64):
        if board[i] == "P":
            white_pawn_control = white_pawn_control + pawn_control(i,board,True)
        if board[i] == "R":
            white_rook_control = white_rook_control + rook_control(i,board,True)
        if board[i] == "N":
            white_knight_control = white_knight_control + knight_control(i,board,True)
        if board[i] == "B":
            white_bishop_control = white_bishop_control + bishop_control(i,board,True)
        if board[i] == "Q":
            white_queen_control = white_queen_control + queen_control(i,board,True)
        if board[i] == "K":
            white_king_control = white_king_control + king_control(i,board,True)
        if board[i] == "p":
            black_pawn_control = black_pawn_control + pawn_control(i,board,False)
        if board[i] == "r":
            black_rook_control = black_rook_control + rook_control(i,board,False)
        if board[i] == "n":
            black_knight_control = black_knight_control + knight_control(i,board,False)
        if board[i] == "b":
            black_bishop_control = black_bishop_control + bishop_control(i,board,False)
        if board[i] == "q":
            black_queen_control = black_queen_control + queen_control(i,board,False)
        if board[i] == "k":
            black_king_control = black_king_control + king_control(i,board,False)
    if color == True:
        control_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        danger_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "P":
                if i in danger_position:
                    modify1 = 0.5
                if i in control_position:
                    modify2 = 1.5
                if row == 3:
                    modify3 = 1.1
                elif row == 4:
                    modify3 = 1.15
                elif row == 5:
                    modify3 = 1.3
                elif row == 6:
                    modify3 = 1.5
                elif row == 7:
                    modify3 = 1.7
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "K":
                if row in [1,2]:
                    modify1 = 1.2
                score = score + 150 * modify1
            elif piece == "Q":
                if row in [6,5]:
                    modify1 = 1.5
                elif row in [4,7]:
                    modify1 = 1.3
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "RNBQKP":
                            modify2 = 1
                        elif board[j] in "rnbqkp":
                            modify2 = 0.5
                        else:
                            modify2 = 0.7
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.3
                    else:
                        modify3 = 0.1
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
            elif piece == "R":
                if row in [7,8]:
                    modify1 = 1.5
                if row in [5,6]:
                    modify1 = 1.3
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
            elif piece == "N":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0.1
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
            elif piece == "B":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0.1
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify2 * modify1

            elif board[i] == "p":
                if i-8 in white_queen_control:
                    score = score - 20
                if row == 4:
                    modify1 == 1.2
                if row == 3:
                    modify1 == 1.4
                if row == 2:
                    if board[i-8] == " ":
                        modify1 == 5
                    elif col >= 2:
                        if board[i-9] in "PRNBQK":
                            modify1 == 5
                    elif col <= 7:
                        if board[i-7] in "PRNBQK":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "k":
                if i in control_position:
                    score = score + 20
                score = score - 150
            elif board[i] == "q":
                score = score - 100
            elif board[i] == "r":
                score = score - 51
            elif board[i] == "n":
                score = score - 49
            elif board[i] == "b":
               score = score - 50
        return score
    if color == False:
        control_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        danger_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "p":
                if i in danger_position:
                    modify1 = 0.5
                if i in control_position:
                    modify2 = 1.5
                if row == 3:
                    modify3 = 1.1
                elif row == 4:
                    modify3 = 1.15
                elif row == 5:
                    modify3 = 1.3
                elif row == 6:
                    modify3 = 1.5
                elif row == 7:
                    modify3 = 1.7
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "k":
                if row in [1,2]:
                    modify1 = 1.2
                score = score + 150 * modify1
            elif piece == "q":
                if row in [3,4]:
                    modify1 = 1.5
                elif row in [2,5]:
                    modify1 = 1.3
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "RNBQKP":
                            modify2 = 1
                        elif board[j] in "rnbqkp":
                            modify2 = 0.5
                        else:
                            modify2 = 0.7
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.3
                    else:
                        modify3 = 0.1
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
            elif piece == "r":
                if row in [1,2]:
                    modify1 = 1.5
                if row in [3,4]:
                    modify1 = 1.3
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
            elif piece == "n":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0.1
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
            elif piece == "b":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0.1
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify2 * modify1

            elif board[i] == "P":
                if i-8 in white_queen_control:
                    score = score - 20
                if row == 4:
                    modify1 == 1.2
                if row == 3:
                    modify1 == 1.4
                if row == 2:
                    if board[i-8] == " ":
                        modify1 == 5
                    elif col >= 2:
                        if board[i-9] in "prnbqk":
                            modify1 == 5
                    elif col <= 7:
                        if board[i-7] in "prnbqk":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "K":
                if i in control_position:
                    score = score + 20
                score = score - 150
            elif board[i] == "Q":
                score = score - 100
            elif board[i] == "R":
                score = score - 51
            elif board[i] == "N":
                score = score - 49
            elif board[i] == "B":
               score = score - 50
        return score

def evaluate_function_ed(board,color):
    for i in range(64):
        if board[i] == "K":
            white_king_position = i
            break
    white_king_row = white_king_position//8+1
    white_king_col = white_king_position-8*(white_king_row-1)+1
    for i in range(64):
        if board[i] == "k":
            black_king_position = i
            break
    black_king_row = black_king_position//8+1
    black_king_col = black_king_position-8*(black_king_row-1)+1
    score = 0
    white_pawn_control = []
    white_rook_control = []
    white_knight_control = []
    white_bishop_control = []
    white_queen_control = []
    white_king_control = []
    black_pawn_control = []
    black_rook_control = []
    black_knight_control = []
    black_bishop_control = []
    black_queen_control = []
    black_king_control = []
    for i in range(64):
        if board[i] == "P":
            white_pawn_control = white_pawn_control + pawn_control(i,board,True)
        if board[i] == "R":
            white_rook_control = white_rook_control + rook_control(i,board,True)
        if board[i] == "N":
            white_knight_control = white_knight_control + knight_control(i,board,True)
        if board[i] == "B":
            white_bishop_control = white_bishop_control + bishop_control(i,board,True)
        if board[i] == "Q":
            white_queen_control = white_queen_control + queen_control(i,board,True)
        if board[i] == "K":
            white_king_control = white_king_control + king_control(i,board,True)
        if board[i] == "p":
            black_pawn_control = black_pawn_control + pawn_control(i,board,False)
        if board[i] == "r":
            black_rook_control = black_rook_control + rook_control(i,board,False)
        if board[i] == "n":
            black_knight_control = black_knight_control + knight_control(i,board,False)
        if board[i] == "b":
            black_bishop_control = black_bishop_control + bishop_control(i,board,False)
        if board[i] == "q":
            black_queen_control = black_queen_control + queen_control(i,board,False)
        if board[i] == "k":
            black_king_control = black_king_control + king_control(i,board,False)
    if color == True:
        control_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        danger_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "P":
                if i in danger_position:
                    modify1 = 0.5
                if i in control_position:
                    modify2 = 1.5
                if row == 3:
                    modify3 = 1.1
                elif row == 4:
                    modify3 = 1.15
                elif row == 5:
                    modify3 = 1.3
                elif row == 6:
                    modify3 = 1.5
                elif row == 7:
                    modify3 = 1.7
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "K":
                if row in [1,2]:
                    modify1 = 1.2
                score = score + 150 * modify1
                modify4 = modify4 * (1 + 0.0001 * (14-(abs(row-black_king_row)+abs(col-black_king_col))))
            elif piece == "Q":
                if row in [6,5]:
                    modify1 = 1.5
                elif row in [4,7]:
                    modify1 = 1.3
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "RNBQKP":
                            modify2 = 1
                        elif board[j] in "rnbqkp":
                            modify2 = 0.5
                        else:
                            modify2 = 0.7
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.3
                    else:
                        modify3 = 0                  
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
                modify4 = modify4 * (1 + 0.0005 * (14-(abs(row-black_king_row)+abs(col-black_king_col))))
            elif piece == "R":
                if row in [7,8]:
                    modify1 = 1.5
                if row in [5,6]:
                    modify1 = 1.3
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
                modify4 = modify4 * (1 + 0.0002 * (14-(abs(row-black_king_row)+abs(col-black_king_col))))
            elif piece == "N":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
                modify4 = modify4 * (1 + 0.0002 * (14-(abs(row-black_king_row)+abs(col-black_king_col))))
            elif piece == "B":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify2 * modify1
                modify4 = modify4 * (1 + 0.002 * (14-(abs(row-black_king_row)+abs(col-black_king_col))))

            elif board[i] == "p":
                if i-8 in white_queen_control:
                    score = score - 20
                if row == 4:
                    modify1 == 1.2
                if row == 3:
                    modify1 == 1.4
                if row == 2:
                    if board[i-8] == " ":
                        modify1 == 5
                    elif col >= 2:
                        if board[i-9] in "PRNBQK":
                            modify1 == 5
                    elif col <= 7:
                        if board[i-7] in "PRNBQK":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "k":
                if i in control_position:
                    score = score + 50
                score = score - 150
            elif board[i] == "q":
                score = score - 100
            elif board[i] == "r":
                score = score - 51
            elif board[i] == "n":
                score = score - 49
            elif board[i] == "b":
               score = score - 50
        if score > 0:
            return score * modify4
        else:
            return score
    if color == False:
        control_position = black_pawn_control + black_rook_control + black_knight_control + black_bishop_control + black_queen_control + black_king_control
        danger_position = white_pawn_control + white_rook_control + white_knight_control + white_bishop_control + white_queen_control + white_king_control
        if is_checkmated(board, not color) == True:
            return -10000
        for i in range(64):
            modify1 = 1
            modify2 = 1
            modify3 = 1
            modify4 = 1
            piece = board[i]
            row = i//8+1
            col = i-8*(row-1)+1
            if piece == "P":
                if i in danger_position:
                    modify1 = 0.5
                if i in control_position:
                    modify2 = 1.5
                if row == 3:
                    modify3 = 1.1
                elif row == 4:
                    modify3 = 1.15
                elif row == 5:
                    modify3 = 1.3
                elif row == 6:
                    modify3 = 1.5
                elif row == 7:
                    modify3 = 1.7
                score = score + 20 * modify1 * modify2 * modify3
            elif piece == "K":
                if row in [7,8]:
                    modify1 = 1.2
                score = score + 150 * modify1
                modify4 = modify4 * (1 + 0.00005 * (14-(abs(row-white_king_row)+abs(col-white_king_col))))
            elif piece == "Q":
                if row in [3,4]:
                    modify1 = 1.5
                elif row in [2,5]:
                    modify1 = 1.3
                for j in white_queen_control:
                    if board[j] in danger_position:
                        if board[j] in "rnbqkp":
                            modify2 = 1
                        elif board[j] in "RNBQKP":
                            modify2 = 0.5
                        else:
                            modify2 = 0.7
                if i in danger_position:
                    if i in control_position:
                        modify3 = 0.3
                    else:
                        modify3 = 0                  
                elif i in control_position:
                    modify3 = 1.1
                score = score + 100 * modify1 * modify2 * modify3
                modify4 = modify4 * (1 + 0.0005 * (14-(abs(row-white_king_row)+abs(col-white_king_col))))
            elif piece == "R":
                if row in [1,2]:
                    modify1 = 1.5
                if row in [3,4]:
                    modify1 = 1.3
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
                modify4 = modify4 * (1 + 0.0002 * (14-(abs(row-white_king_row)+abs(col-white_king_col))))
            elif piece == "N":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify1 * modify2
                modify4 = modify4 * (1 + 0.0002 * (14-(abs(row-white_king_row)+abs(col-white_king_col))))
            elif piece == "B":
                if row+col in [8,9,10]:
                    modify1 = 1.3
                elif row+col in [6,7,11,12]:
                    modify1 = 1.2
                elif row+col in [4,5,13,14]:
                    modify1 = 1.1
                if i in danger_position:
                    if i in control_position:
                        modify2 = 0.3
                    else:
                        modify2 = 0
                elif i in control_position:
                    modify2 = 1.2
                score = score + 50 * modify2 * modify1
                modify4 = modify4 * (1 + 0.002 * (14-(abs(row-white_king_row)+abs(col-white_king_col))))

            elif board[i] == "p":
                if i-8 in white_queen_control:
                    score = score - 20
                if row == 4:
                    modify1 == 1.2
                if row == 3:
                    modify1 == 1.4
                if row == 2:
                    if board[i-8] == " ":
                        modify1 == 5
                    elif col >= 2:
                        if board[i-9] in "PRNBQK":
                            modify1 == 5
                    elif col <= 7:
                        if board[i-7] in "PRNBQK":
                            modify1 == 5
                score = score - 20 * modify1
            elif board[i] == "k":
                if i in control_position:
                    score = score + 50
                score = score - 150
            elif board[i] == "q":
                score = score - 100
            elif board[i] == "r":
                score = score - 51
            elif board[i] == "n":
                score = score - 49
            elif board[i] == "b":
               score = score - 50
        if score > 0:
            return score * modify4
        else:
            return score

def control_recursive_check(position,move_number,board,color,maximum_recursive):
    t_position = position
    control = []
    opponent_chess = opponent_chesses(color)
    my_chess = my_chesses(color)
    for i in range(1,maximum_recursive+1):
        t_position = t_position + move_number
        if board[t_position] in my_chess: 
            control = control + [t_position]
            break
        if board[t_position] == " ":
            control = control + [t_position]
        if board[t_position] in opponent_chess: 
            control = control + [t_position]
            break
    return control

def pawn_control(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    control_position = []
    if color == True:
        control_position = control_position + [position+8]
        if col >= 2:
            control_position = control_position + [position+7]
        if col <= 7:
            control_position = control_position + [position+9]
        if row == 2:
            control_position = control_position + [position+16]
    else:
        control_position = control_position + [position-8]
        if col >= 2:
            control_position = control_position + [position-9]
        if col <= 7:
            control_position = control_position + [position-7]
        if row == 7:
            control_position = control_position + [position-16]
    return control_position

def rook_control(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    up = control_recursive_check(position,8,board,color,up_recursive)
    down = control_recursive_check(position,-8,board,color,down_recursive)
    right = control_recursive_check(position,1,board,color,right_recursive)
    left = control_recursive_check(position,-1,board,color,left_recursive)
    control = up + down + right + left
    return control

def bishop_control(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    upright_recursive = min(up_recursive, right_recursive)
    upleft_recursive = min(up_recursive, left_recursive)
    downright_recursive = min(down_recursive, right_recursive)
    downleft_recursive = min(down_recursive, left_recursive)
    upright = control_recursive_check(position,9,board,color,upright_recursive)
    upleft = control_recursive_check(position,7,board,color,upleft_recursive)
    downright = control_recursive_check(position,-7,board,color,downright_recursive)
    downleft = control_recursive_check(position,-9,board,color,downleft_recursive)
    control = upright + upleft + downright + downleft
    return control

def knight_control(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    control = []
    if row > 1:
        if col > 2:
            control = control + [position-10]
        if col < 7:
            control = control + [position-6]
    if row > 2:
        if col > 1:
            control = control + [position-17]
        if col < 8:
            control = control + [position-15]
    if row < 8:
        if col > 2:
            control = control + [position+6]
        if col < 7:
            control = control + [position+10]
    if row < 7:
        if col > 1:
            control = control + [position+15]
        if col < 8:
            control = control + [position+17]
    return control

def queen_control(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    upright_recursive = min(up_recursive, right_recursive)
    upleft_recursive = min(up_recursive, left_recursive)
    downright_recursive = min(down_recursive, right_recursive)
    downleft_recursive = min(down_recursive, left_recursive)
    upright = control_recursive_check(position,9,board,color,upright_recursive)
    upleft = control_recursive_check(position,7,board,color,upleft_recursive)
    downright = control_recursive_check(position,-7,board,color,downright_recursive)
    downleft = control_recursive_check(position,-9,board,color,downleft_recursive)
    up = control_recursive_check(position,8,board,color,up_recursive)
    down = control_recursive_check(position,-8,board,color,down_recursive)
    right = control_recursive_check(position,1,board,color,right_recursive)
    left = control_recursive_check(position,-1,board,color,left_recursive)
    control = up + down + right + left + upright + upleft + downright + downleft
    return control

def king_control(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    control = []
    if row > 1:
        control = control + [position-8]
        if col > 1:
            control = control + [position-9]
        if col < 8:
            control = control + [position-7]
    if row < 8:
        control = control + [position+8]
        if col > 1:
            control = control + [position+7]
        if col < 8:
            control = control + [position+9]
    if col > 1:
        control = control + [position-1]
    if col < 8:
        control = control + [position+1]
    return control

def visualize(board):

    return None

def opponent_chesses(color):
    if color:
        return "rnbqkp"
    return "RNBQKP" 

def my_chesses(color):
    if color:
        return "RNBQKP"
    return "rnbqkp"

def position_to_coordinator(position):
    row = position//8+1
    col = position-8*(row-1)+1
    return lettertonumber[col-1]+str(row)

def start_game(color):

    if color:
        board = startboard_white
        print("b2b3")
        return board
    else:
        board = startboard_black
        return board

def make_a_move(board,move): 

    s_alpha = lettertonumber.find(move[0])
    s_number = int(move[1])-1
    s_position = 8 * s_number + s_alpha
    t_alpha = lettertonumber.find(move[2])
    t_number = int(move[3])-1
    t_position = 8 * t_number + t_alpha
    newboard = board.copy()
    if move == "e1g1":
        if board[4] == "K":
            newboard[5] = "R"
            newboard[6] = "K"
            newboard[7] = " "
            newboard[-5] = False
            newboard[-6] = False
        else:
            newboard[t_position] = board[s_position]
    elif move == "e1c1":
        if board[4] == "K":
            newboard[3] = "R"
            newboard[2] = "K"
            newboard[0] = " "
            newboard[-5] = False
            newboard[-6] = False
        else:
            newboard[t_position] = board[s_position]
    elif move == "e8g8":
        if board[60] == "k":
            newboard[61] = "r"
            newboard[62] = "k"
            newboard[63] = " "
            newboard[-3] = False
            newboard[-4] = False
        else:
            newboard[t_position] = board[s_position]
    elif move == "e8c8":
        if board[60] == "k":
            newboard[59] = "r"
            newboard[58] = "k"
            newboard[56] = " "
            newboard[-3] = False
            newboard[-4] = False
        else:
            newboard[t_position] = board[s_position]
    elif len(move) == 5:
        if board[s_position] == "p":
            newboard[t_position] = move[4]
        if board[s_position] == "P":
            if move[4] == "q":
                newboard[t_position] = "Q"
            if move[4] == "r":
                newboard[t_position] = "R"
            if move[4] == "b":
                newboard[t_position] = "B"
            if move[4] == "n":
                newboard[t_position] = "N"
    else:
        newboard[t_position] = board[s_position]
    newboard[s_position] = " "
    if board[s_position] == "P":
        if t_position == 39 + board[-2]:
            newboard[31 + board[-2]] = " "
    if board[s_position] == "p":
        if t_position == 15 + board[-1]:
            newboard[23 + board[-1]] = " "
    if board[s_position] == "P":
        if move == "a2a4":
            newboard[-1] = 1
        elif move == "b2b4":
            newboard[-1] = 2
        elif move == "c2c4":
            newboard[-1] = 3
        elif move == "d2d4":
            newboard[-1] = 4
        elif move == "e2e4":
            newboard[-1] = 5
        elif move == "f2f4":
            newboard[-1] = 6
        elif move == "g2g4":
            newboard[-1] = 7
        elif move == "h2h4":
            newboard[-1] = 8
        else:
            newboard[-1] = 0
    elif board[s_position] == "p":
        if move == "a7a5":
            newboard[-2] = 1
        elif move == "b7b5":
            newboard[-2] = 2
        elif move == "c7c5":
            newboard[-2] = 3
        elif move == "d7d5":
            newboard[-2] = 4
        elif move == "e7e5":
            newboard[-2] = 5
        elif move == "f7f5":
            newboard[-2] = 6
        elif move == "g7g5":
            newboard[-2] = 7
        elif move == "h7h5":
            newboard[-2] = 8   
        else:
            newboard[-2] = 0     
    return newboard

def next_move(board,color,n,normal):
    possible_move = legal_move(board,color)
    if possible_move == []:
        return "lose"
    length = len(possible_move)
    if length == 1:
        return [possible_move[0],False]
    if n <= 20:
        possible_move.sort(key=lambda x: evaluate_function_op(make_a_move(board,x),color),reverse=True)
    elif n < 40:
        possible_move.sort(key=lambda x: evaluate_function_md(make_a_move(board,x),color),reverse=True)
    else:
        possible_move.sort(key=lambda x: evaluate_function_ed(make_a_move(board,x),color),reverse=True)
    if n <= 30:
        if normal == True:
            if color == True:
                if white_normal_start[n] in possible_move:
                    t_alpha = lettertonumber.find(white_normal_start[n][2])
                    t_number = int(white_normal_start[n][3])-1
                    t_position = 8 * t_number + t_alpha
                    if t_position in danger_position(make_a_move(board,white_normal_start[n]),color):
                        return [possible_move[0],False]
                    else:
                        return [white_normal_start[n],True]
            if color == False:
                if black_normal_start[n-1] in possible_move:
                    t_alpha = lettertonumber.find(black_normal_start[n-1][2])
                    t_number = int(black_normal_start[n-1][3])-1
                    t_position = 8 * t_number + t_alpha
                    if t_position in danger_position(board,color):
                        return [possible_move[0],False]
                    else:
                        return [black_normal_start[n-1],True]
    return [possible_move[0],False]
    
def is_checkmated(board,color):
    if legal_move(board,color) == []:
        return True
    else:
        return False

def recursive_check(position,move_number,board,color,maximum_recursive):
    t_position = position
    capture = []
    move = []
    opponent_chess = opponent_chesses(color)
    my_chess = my_chesses(color)
    for i in range(1,maximum_recursive+1):
        t_position = t_position + move_number
        end_position = position_to_coordinator(t_position)
        if board[t_position] in my_chess:
            break
        if board[t_position] == " ":
            move = move + [end_position]
        if board[t_position] in opponent_chess:
            capture = capture + [end_position]
            break
    return [capture, move]

def nonrecursive_check(position,move_number,board,color):
    t_position = position
    capture = []
    move = []
    opponent_chess = opponent_chesses(color)
    t_position = t_position + move_number
    end_position = position_to_coordinator(t_position)
    if board[t_position] == " ":
        move = [end_position]
    if board[t_position] in opponent_chess:
        capture = [end_position]
    return [capture, move]

def legal_move_pawn(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    capture = []
    move = []
    start_position = position_to_coordinator(position)
    if color == True:
        if board[-2] != 0:
            if row == 5:
                if col == board[-2] + 1:
                    end_position = position_to_coordinator(position+7)
                    a_capture = start_position + end_position
                    capture = capture + [a_capture]
                if col == board[-2] - 1:
                    end_position = position_to_coordinator(position+9)
                    a_capture = start_position + end_position
                    capture = capture + [a_capture]
        if board[position+8] == " ": 
            end_position = position_to_coordinator(position+8)
            amove = start_position + end_position
            move = move + [amove]
            if row == 2: 
                if board[position+16] == " ":
                    end_position = position_to_coordinator(position+16)
                    amove = start_position + end_position
                    move = move + [amove]
        if col > 1: 
            if board[position+7] in "rnbqkp":
                end_position = position_to_coordinator(position+7)
                a_capture = start_position + end_position
                capture = capture + [a_capture]
        if col < 8: 
            if board[position+9] in "rnbqkp":
                end_position = position_to_coordinator(position+9)
                a_capture = start_position + end_position
                capture = capture + [a_capture]
        if row == 7:
            p_capture = []
            p_move = []
            for single_move in capture:
                p_capture = p_capture + [single_move+"q",single_move+"r",single_move+"n",single_move+"k"]
            for single_move in move:
                p_move = p_move + [single_move+"q",single_move+"r",single_move+"n",single_move+"k"]
            return [p_capture,p_move]
        else:
            return [capture, move]
    if color == False:
        if board[-1] != 0:
            if row == 4:
                if col == board[-1] + 1:
                    end_position = position_to_coordinator(position-9)
                    a_capture = start_position + end_position
                    capture = capture + [a_capture]
                if col == board[-1] - 1:
                    end_position = position_to_coordinator(position-7)
                    a_capture = start_position + end_position
                    capture = capture + [a_capture]
        if board[position-8] == " ": 
            end_position = position_to_coordinator(position-8)
            amove = start_position + end_position
            move = move + [amove]
            if row == 7: 
                if board[position-16] == " ":
                    end_position = position_to_coordinator(position-16)
                    amove = start_position + end_position
                    move = move + [amove]
        if col > 1: 
            if board[position-9] in "RNBQKP":
                end_position = position_to_coordinator(position-9)
                a_capture = start_position + end_position
                capture = capture + [a_capture]
        if col < 8: 
            if board[position-7] in "RNBQKP":
                end_position = position_to_coordinator(position-7)
                a_capture = start_position + end_position
                capture = capture + [a_capture]
        if row == 2:
            p_capture = []
            p_move = []
            for single_move in capture:
                p_capture = p_capture + [single_move+"q",single_move+"r",single_move+"n",single_move+"b"]
            for single_move in move:
                p_move = p_move + [single_move+"q",single_move+"r",single_move+"n",single_move+"b"]
            return [p_capture,p_move]
        else:
            return [capture, move]

def legal_move_rook(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    start_position = position_to_coordinator(position)
    up = recursive_check(position,8,board,color,up_recursive)
    down = recursive_check(position,-8,board,color,down_recursive)
    right = recursive_check(position,1,board,color,right_recursive)
    left = recursive_check(position,-1,board,color,left_recursive)
    captures = up[0] + down[0] + right[0] + left[0]
    moves = up[1] + down[1] + right[1] + left[1]
    m = list(map(lambda x: start_position + x, moves))
    c = list(map(lambda x: start_position + x, captures))
    return [c, m]

def legal_move_bishop(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    upright_recursive = min(up_recursive, right_recursive)
    upleft_recursive = min(up_recursive, left_recursive)
    downright_recursive = min(down_recursive, right_recursive)
    downleft_recursive = min(down_recursive, left_recursive)
    start_position = position_to_coordinator(position)
    upright = recursive_check(position,9,board,color,upright_recursive)
    upleft = recursive_check(position,7,board,color,upleft_recursive)
    downright = recursive_check(position,-7,board,color,downright_recursive)
    downleft = recursive_check(position,-9,board,color,downleft_recursive)
    captures = upright[0] + upleft[0] + downright[0] + downleft[0]
    moves = upright[1] + upleft[1] + downright[1] + downleft[1]
    m = list(map(lambda x: start_position + x, moves))
    c = list(map(lambda x: start_position + x, captures))
    return [c, m]

def legal_move_knight(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    capture = []
    move = []
    start_position = position_to_coordinator(position)
    if row > 1:
        if col > 2:
            amove = nonrecursive_check(position,-10,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 7:
            amove = nonrecursive_check(position,-6,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    if row > 2:
        if col > 1:
            amove = nonrecursive_check(position,-17,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 8:
            amove = nonrecursive_check(position,-15,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    if row < 8:
        if col > 2:
            amove = nonrecursive_check(position,+6,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 7:
            amove = nonrecursive_check(position,+10,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    if row < 7:
        if col > 1:
            amove = nonrecursive_check(position,15,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 8:
            amove = nonrecursive_check(position,17,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    m = list(map(lambda x: start_position + x, move))
    c = list(map(lambda x: start_position + x, capture))
    return [c, m]

def legal_move_queen(position,board,color):
    down_recursive = position//8
    up_recursive = 7 - down_recursive
    left_recursive = position - 8 * down_recursive
    right_recursive = 7 - left_recursive
    upright_recursive = min(up_recursive, right_recursive)
    upleft_recursive = min(up_recursive, left_recursive)
    downright_recursive = min(down_recursive, right_recursive)
    downleft_recursive = min(down_recursive, left_recursive)
    start_position = position_to_coordinator(position)
    upright = recursive_check(position,9,board,color,upright_recursive)
    upleft = recursive_check(position,7,board,color,upleft_recursive)
    downright = recursive_check(position,-7,board,color,downright_recursive)
    downleft = recursive_check(position,-9,board,color,downleft_recursive)
    up = recursive_check(position,8,board,color,up_recursive)
    down = recursive_check(position,-8,board,color,down_recursive)
    right = recursive_check(position,1,board,color,right_recursive)
    left = recursive_check(position,-1,board,color,left_recursive)
    captures = up[0] + down[0] + right[0] + left[0] + upright[0] + upleft[0] + downright[0] + downleft[0]
    moves = up[1] + down[1] + right[1] + left[1] + upright[1] + upleft[1] + downright[1] + downleft[1]
    m = list(map(lambda x: start_position + x, moves))
    c = list(map(lambda x: start_position + x, captures))
    return [c, m]

def legal_move_king(position,board,color):
    row = position//8+1
    col = position-8*(row-1)+1
    capture = []
    move = []
    start_position = position_to_coordinator(position)
    if row > 1:
        amove = nonrecursive_check(position,-8,board,color)
        move = move + amove[1]
        capture = capture + amove[0]
        if col > 1:
            amove = nonrecursive_check(position,-9,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 8:
            amove = nonrecursive_check(position,-7,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    if row < 8:
        amove = nonrecursive_check(position,8,board,color)
        move = move + amove[1]
        capture = capture + amove[0]
        if col > 1:
            amove = nonrecursive_check(position,7,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
        if col < 8:
            amove = nonrecursive_check(position,9,board,color)
            move = move + amove[1]
            capture = capture + amove[0]
    if col > 1:
        amove = nonrecursive_check(position,-1,board,color)
        move = move + amove[1]
        capture = capture + amove[0]
    if col < 8:
        amove = nonrecursive_check(position,1,board,color)
        move = move + amove[1]
        capture = capture + amove[0]
    m = list(map(lambda x: start_position + x, move))
    c = list(map(lambda x: start_position + x, capture))
    return [c, m]

def danger_position(board, color):
    forbid_position = []
    opponent_color = not color
    captures = possiblemove(board,opponent_color)
    for move in captures[0]:
        t_alpha = lettertonumber.find(move[2])
        t_number = int(move[3])-1
        t_position = 8 * t_number + t_alpha
        forbid_position = forbid_position + [t_position]
    return forbid_position

def is_checkmated(board,color):
    if color == True:
        for i in danger_position(board, color):
            if board[i] == "K":
                return True
    if color == False:
        for i in danger_position(board, color):
            if board[i] == "k":
                return True
    return False

def possiblemove(board,color):
    capture = []
    move = []
    if color:
        for i in range(64):
            if board[i] == "P":
                capture_and_move = legal_move_pawn(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "R":
                capture_and_move = legal_move_rook(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "B":
                capture_and_move = legal_move_bishop(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "N":
                capture_and_move = legal_move_knight(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "Q":
                capture_and_move = legal_move_queen(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "K":
                capture_and_move = legal_move_king(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
    else:
        for i in range(64):
            if board[i] == "p":
                capture_and_move = legal_move_pawn(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "r":
                capture_and_move = legal_move_rook(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "b":
                capture_and_move = legal_move_bishop(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "n":
                capture_and_move = legal_move_knight(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "q":
                capture_and_move = legal_move_queen(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
            if board[i] == "k":
                capture_and_move = legal_move_king(i,board,color)
                capture = capture + capture_and_move[0]
                move = move + capture_and_move[1]
    return [capture,move]

def legal_move(board,color):
    possible_move = possiblemove(board,color)
    legal_moves = []
    for amove in possible_move[0]:
        potential_board = make_a_move(board, amove)
        if is_checkmated(potential_board,color) == False:
            legal_moves = legal_moves + [amove]
    if legal_moves == []:
        for amove in possible_move[1]:
            potential_board = make_a_move(board, amove)
            if is_checkmated(potential_board,color) == False:
                legal_moves = legal_moves + [amove]
    return legal_moves



a = sys.argv
if len(a) == 2:
    strcolor = sys.argv[1]
else:
    strcolor = input()

color = (strcolor == "white")

board = start_game(color)

nth_move = 1
normal = True
while True:
    move = input()
    if move == "stop":
        break
    board = make_a_move(board,move)
    a = next_move(board,color,nth_move,normal)
    move = a[0]
    normal = a[1]
    if move == "lose":
        print("lose")
        break
    print(move)
    board = make_a_move(board,move)
    nth_move = nth_move + 1


